import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

public class Main {

    private final String AppID = "82dd60fff552caa545a1402df6a836be";


    private final static StringBuilder builder = new StringBuilder();

    private final static boolean isTest = false;

    public static void main(String[] args){
        List<ParcableElement> parcableElementList = new ArrayList<>();
        parcableElementList.add(new ParcableElement("name", 1, "name", ParcableElement.ValueType.TEXT, "name"));
        Loader gunloader = new Loader("guns", parcableElementList, "tankguns");
        System.out.println(gunloader.toString());

        parcableElementList.add(new ParcableElement("nation", 1,"country", ParcableElement.ValueType.TEXT, "nation"));
        parcableElementList.add(new ParcableElement("type", 1, "type", ParcableElement.ValueType.TEXT, "type"));
        parcableElementList.add(new ParcableElement("tier", 1, "tier", ParcableElement.ValueType.NUMBER, "tier"));

        parcableElementList.add(new ParcableElement("front", 1, "armor_body_front", ParcableElement.ValueType.NUMBER, "default_profile.armor.hull"));
        parcableElementList.add(new ParcableElement("sides", 1, "armor_body_sides", ParcableElement.ValueType.NUMBER, "default_profile.armor.hull"));
        parcableElementList.add(new ParcableElement("penetration", 1, "penetration", ParcableElement.ValueType.NUMBER, "default_profile.ammo.penetration"));

        Loader vehisleLoader = new Loader("dbo.vehicles", parcableElementList,"vehicles");

        System.out.println(vehisleLoader.apiUrl);
        System.out.println(gunloader.apiUrl);

        URL wotUrl = null;
        try {
            wotUrl = new URL("https://api.worldoftanks.ru/wot/encyclopedia/vehicles/?application_id=82dd60fff552caa545a1402df6a836be&fields=nation%2Ctier%2Cname%2Ctank_id%2Ctype%2Cdefault_profile.armor.hull%2Cdefault_profile.ammo.penetration&language=ru");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        if(wotUrl == null)
            return;

        try {
            URLConnection connection = wotUrl.openConnection();
            BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String line = rd.readLine();
            System.out.println(line);
            String[] pairList;

                //разбиваем по штучно на техику

            for(String vehisleRec: line.split("\"tank_id\":\\d+},")){

                if (isTest) System.out.println(vehisleRec);

                //начинаем формировать для вставки в БД
                builder.setLength(0);
                builder.append(vehisleLoader.insertStatement);

                //разбиваем на пары - имя и значения параметра
                pairList = vehisleRec.split(",");

                for(ParcableElement element: parcableElementList){
                    builder.append(getValue(element, pairList));
                    builder.append(",");
                }

                builder.setLength(builder.length() - 1);

                builder.append(");");

                System.out.println(builder.toString());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
    //todo для первой техники в списке нужно доработать получение наименования
    private static String getValue(ParcableElement elem, String[] list){
        StringBuilder resault = null;
        String res = null;
        for(String pair: list) {
            if (isTest) System.out.println(pair);

            try {
                if (pair.contains(elem.nameForSearch)) {
                    res = pair.substring(pair.indexOf(elem.name)).split(":")[elem.number].replace("}}", "").replace("{\"name\"", "\"неопределено\"");
                }
            } catch (ArrayIndexOutOfBoundsException e){
                res = "exp";
            }
        }

        res = res.replace("+++++++[", "");

        if (elem.valueType == ParcableElement.ValueType.NUMBER){
            try {
                res = String.valueOf(Integer.parseInt(res));
            }catch (Exception e){
                res = "";
            }
        }

        if (elem.valueType == ParcableElement.ValueType.TEXT){
            resault = new StringBuilder("\'");
            resault.append(res.replace("\'",""));
            resault.append("\'");
            res = resault.toString();
        }

        return res;

    }

    static class ParcableElement{

        public enum ValueType{
            TEXT, NUMBER
        }

        public final String name;
        public final String nameForSearch;
        public final int number;
        public final String dbColumnName;
        public final ValueType valueType;
        public final String urlName;


        public ParcableElement(String name, int number, String dbColumnName, ValueType valueType, String urlName) {
            this.name = name;
            this.nameForSearch = "\"" + name + "\"";
            this.number = number;
            this.dbColumnName = dbColumnName;
            this.valueType = valueType;
            this.urlName = urlName;
        }
    }
}
