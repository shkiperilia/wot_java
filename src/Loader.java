import java.util.List;

public class Loader {
    public final String dB_Name;
    public List<Main.ParcableElement> parcableElementList;
    public final String insertStatement;
    public final String apiUrl;
    public final String urlName;

    public Loader(String dB_Name, List<Main.ParcableElement> parcableElementList, String urlName) {
        this.dB_Name = dB_Name;
        this.parcableElementList = parcableElementList;
        this.urlName = urlName;

        StringBuilder insertBuilder = new StringBuilder("insert into " + dB_Name +  "(");
        for(Main.ParcableElement element: parcableElementList){
            insertBuilder.append(element.dbColumnName).append(",");
        }
        insertBuilder.setLength(insertBuilder.length() -1);
        insertBuilder.append(") values (");
        insertStatement = insertBuilder.toString();

        StringBuilder  apiBuilder = new StringBuilder();

        apiBuilder.append("https://api.worldoftanks.ru/wot/encyclopedia/").append(urlName).append("?application_id=82dd60fff552caa545a1402df6a836be&fields=");

        for (Main.ParcableElement field: parcableElementList){
            apiBuilder.append(field.urlName).append("%2C");

        }

        apiBuilder.append("&language=ru");

        apiUrl = apiBuilder.toString();

    }

    @Override
    public String toString() {
        return dB_Name + ", " + insertStatement;
    }
}
